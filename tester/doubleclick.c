///*****************************************************
//Chip type               : ATmega328
//Program type            : Application
//AVR Core Clock frequency: 16.000000 MHz
//Memory model            : Small
//External RAM size       : 0
//Data Stack size         : 512
// *****************************************************/
//
//#include <mega328.h>
//#include <fsm.h>
//
//int state = OFF_STABLE;
//// Timer1 output compare A interrupt service routine
//
//interrupt [TIM1_COMPA] void timer1_compa_isr(void) {
//    int pin_input;
//    int input;
//    int output = 0;
//    pin_input = PIND; // input di PD2
//    if (pin_input & (1 << 2)) {
//        input = 0;
//    } else {
//        input = 1;
//    }
//    fsm(input, &output, &state);
//    if (output == 1) {
//        PORTD |= 1 << 3;    // output di PD3
//    } else {
//        PORTD &= ~(1 << 3);
//    }
//}
//
//// Declare your global variables here
//
//void main(void) {
//    // Declare your local variables here
//
//    // Crystal Oscillator division factor: 1
//#pragma optsize-
//    CLKPR = 0x80;
//    CLKPR = 0x00;
//#ifdef _OPTIMIZE_SIZE_
//#pragma optsize+
//#endif
//
//    // Input/Output Ports initialization
//    // Port B initialization
//    // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
//    // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
//    PORTB = 0x00;
//    DDRB = 0x20; // one output (PB5)
//
//    // Port C initialization
//    // Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
//    // State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
//    PORTC = 0xff;
//    DDRC = 0x00; // all input
//
//    // Port D initialization
//    // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
//    // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
//    PORTD = 0x00;
//    DDRD = 1 << 3; // D3 (PD3) sebagai output
//
//    PORTD = 0xff;
//
//    // Timer/Counter 0 initialization
//    // Clock source: System Clock
//    // Clock value: Timer 0 Stopped
//    // Mode: Normal top=0xFF
//    // OC0A output: Disconnected
//    // OC0B output: Disconnected
//    TCCR0A = 0x00;
//    TCCR0B = 0x00;
//    TCNT0 = 0x00;
//    OCR0A = 0x00;
//    OCR0B = 0x00;
//
//    // Timer/Counter 1 initialization
//    // Clock source: System Clock
//    // Clock value: 62.500 kHz
//    // Mode: CTC top=OCR1A
//    // OC1A output: Discon.
//    // OC1B output: Discon.
//    // Noise Canceler: Off
//    // Input Capture on Falling Edge
//    // Timer1 Overflow Interrupt: Off
//    // Input Capture Interrupt: Off
//    // Compare A Match Interrupt: On
//    // Compare B Match Interrupt: Off
//    TCCR1A = 0x00;
//    TCCR1B = 0x0C;
//    TCNT1H = 0x00;
//    TCNT1L = 0x00;
//    ICR1H = 0x00;
//    ICR1L = 0x00;
//    OCR1AH = 0xF4; // F424 = 62500 (1 Hz)
//    OCR1AL = 0x24;
//
//    // 271 = 625 (100 Hz)
//    OCR1AH = 0x02; // F424 = 62500
//    OCR1AL = 0x71;
//
//    OCR1BH = 0x00;
//    OCR1BL = 0x00;
//
//    // Timer/Counter 2 initialization
//    // Clock source: System Clock
//    // Clock value: Timer2 Stopped
//    // Mode: Normal top=0xFF
//    // OC2A output: Disconnected
//    // OC2B output: Disconnected
//    ASSR = 0x00;
//    TCCR2A = 0x00;
//    TCCR2B = 0x00;
//    TCNT2 = 0x00;
//    OCR2A = 0x00;
//    OCR2B = 0x00;
//
//    // External Interrupt(s) initialization
//    // INT0: Off
//    // INT1: Off
//    // Interrupt on any change on pins PCINT0-7: Off
//    // Interrupt on any change on pins PCINT8-14: Off
//    // Interrupt on any change on pins PCINT16-23: Off
//    EICRA = 0x00;
//    EIMSK = 0x00;
//    PCICR = 0x00;
//
//    // Timer/Counter 0 Interrupt(s) initialization
//    TIMSK0 = 0x00;
//
//    // Timer/Counter 1 Interrupt(s) initialization
//    TIMSK1 = 0x02;
//
//    // Timer/Counter 2 Interrupt(s) initialization
//    TIMSK2 = 0x00;
//
//    // USART initialization
//    // USART disabled
//    UCSR0B = 0x00;
//
//    // Analog Comparator initialization
//    // Analog Comparator: Off
//    // Analog Comparator Input Capture by Timer/Counter 1: Off
//    ACSR = 0x80;
//    ADCSRB = 0x00;
//    DIDR1 = 0x00;
//
//    // ADC initialization
//    // ADC disabled
//    ADCSRA = 0x00;
//
//    // SPI initialization
//    // SPI disabled
//    SPCR = 0x00;
//
//    // TWI initialization
//    // TWI disabled
//    TWCR = 0x00;
//
//    // Global enable interrupts
//#asm("sei")
//
//    while (1) {
//        // Place your code here
//
//    }
//}