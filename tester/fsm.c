#include <stdio.h>
#include <stdlib.h>
#include "fsm.h"

// file ini berisi FSM yang akan diuji

/**
 * FSM under test
 * @param input
 * @param state
 * @param output
 */
void fsm(int input,int *counter, int *state, int *output) {
    
    // algoritma FSM di sini
    switch (*state) {
        case LEDOFF1:
        {
            if (input == 1) {
		*state = LEDOFF2;
                *counter = 0;
            }
            if (input == 0) {
		*state = LEDOFF1;
                *counter = 0;
            }
            break;
        }
        case LEDOFF2:
        {
            if (input == 1) {
		*state = LEDON;
                *counter = 0;
            }
            if (input == 0) {
		*state = LEDOFF2;
                (*counter)++;
                if (*counter > TIMEOUT_CLICK) {
                    *state = LEDOFF1;
                    *counter = 0;
                }
            }
            break;
        }
        case LEDON:
        {
             if (input == 1) {
		*state = LEDON;
                *counter = 0;
            }
            if (input == 0) {
		*state = LEDON;
                (*counter)++;
                if (*counter > TIMEOUT_ON) {
                    *state = LEDOFF1;
                    *counter = 0;
                }
            }
            break;
        }
        default:
        {

        }
    }
    // perhitungan output
    switch (*state) {
        case LEDOFF1:
        case LEDOFF2:
        {
            *output = 0;
            break;
        }
        case LEDON:
        {
            *output = 1;            
            break;
        }
        default:
        {

        }
    }
}

void ispush(int input, int *output, int *stateevent){
  switch (*stateevent){
    case NOT_PUSH:
    {
      if (input==1){
        *stateevent = PUSH;
        *output=1;
      } 
      if (input==0){
        *stateevent = NOT_PUSH;
      } 
      break;
    }
   case PUSH:
   {
      if (input==0){
        *stateevent = NOT_PUSH;
      } 
      if (input==1){
        *stateevent = PUSH;
      } 
      *output=0;
      break;
   }
  }
}