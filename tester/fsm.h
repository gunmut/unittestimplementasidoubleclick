/* 
 * File:   fsm.h
 * Author: admin
 *
 * Created on November 16, 2014, 5:24 PM
 */

#ifndef FSM_H
#define	FSM_H

#define LEDOFF1 0
#define LEDOFF2 1
#define LEDON 2

#define NOT_PUSH 3
#define PUSH 4

#define TIMEOUT_CLICK 1000
#define TIMEOUT_ON 5000

#ifdef	__cplusplus
extern "C" {
#endif

//void fsm(int input, int *counter, int *state, int *output);
void fsm(int input, int *counter, int *state, int *output);
void ispush(int input, int *output, int *stateevent);


#ifdef	__cplusplus
}
#endif

#endif	/* FSM_H */

